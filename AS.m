clear all
clc
%active set method
% find function
% intersect function
% setdiff function
% union function
% example
% min x1^2+x2^2-2*x1-4*x2---f
% s.t. x1      >=0
%           x2 >=0
%       -x1-x2+1>=0
%AM=[1,0,01;0,1,-1] . b=[0 0 -1]

syms x1 x2
%=============the change of the coefficient=============
%%objective function
% %ex8
%f=0.5*x1^2+0.5*x2^2+x2+2*x1;
%example 5
f=x1^2+x2^2-x1*x2-3*x1;

g=jacobian(f,[x1;x2]);
G=jacobian([g(1);g(2)],[x1,x2]);

%Initial Value x_0
% %ex8
%x_0=[5;0];
%example 5
x_0=[0;0];

% %ex8
% AM=[1,0,-1,1,-1,0;1,-1,-1,-1,0,1] %constraint Coefficient Matrix
% b=[0 -2 -5 -2 -5 -1]; %Constant right hand value
% I=[0 0 3 0 5 0]; % initial active set
%example 5
AM=[-1,1,0,-1;-1,0,1,0]
b=[-2,0,0,-3/2]
I=[0 2 3 0];
% # of constraints
an=size(AM,2);
%======================================================
% %find the active set in I
I1=find(I);
% % Define Matrix A
% % The Number Of Available Aggregate
n=size(I1,2);
% % I1(i): the active set
% % output A according to I1(i) AM(:,I1(i))
A=[];
for i=1:n
    A=[A AM(:,I1(i))];
end
A
%%
%Circulating Body
k=0;
N=10;
while k<N
    k
    X=x_0;
    c=0;
    for i=1:an
        if X'*AM(:,i)<b(i)
            c=c+1;
        else
            c=c;
        end
    end
    c
    
    %if it is in the feasible region
    if c==0
        for i=1:an
            if X'*AM(:,i)==b(i)
                I(i)=i;
            else
                I(i)=0;
            end
        end
        I1=find(I)
        
        g=jacobian(f,[x1;x2]);
        g=subs(g,{x1,x2},{X(1),X(2)});
        % transpose g
        g=g(:)
        G=subs(G,{x1,x2},{X(1),X(2)});
        
        O=zeros(n,n);
        Ap=[G,-A;A',O];
        % bo Denotes Zero Vector In S.t.
        bgn=size(Ap,1)-2;
        bgo=zeros(bgn,1);
        bg=[-g; bgo];
        
        %Solving equations
        r=Ap\bg
        rn=size(r,1);
        %B is u in example
        B=r(3:rn)
        u=zeros(1,an);
        lt=rn-2;
        for i=1:lt
            u(I1(i))=B(i);
        end
        min_u=min(u);
        if min_u>=0
            xbest=x_0;
            break;
        else
            %find the s and let I(s)=0
            for i=1:an
                if u(i)==min_u
                    s=i;
                    break;
                end
            end
            I(s)=0;
            
            I1=setdiff(I1,s)
            n=size(I1,2);
            A=[];
            for i=1:n
                A=[A AM(:,I1(i))];
            end
            A
        end
    end
    
    %======not in feasible region=======
    if  c~=0
        d=x_0-x_1;
        mu=zeros(1,an);
        alpha=zeros(1,an);
        d
        for i=1:an
            if AM(:,i)'*d<0
                mu(i)=i;
            end
            if mu(i)>0
                alpha(i)=(b(mu(i))-AM(:,mu(i))'*x_1)/(AM(:,mu(i))'*d);
            else
                alpha(i)=100;
            end
        end
        maxa=min(alpha)
        x_0=x_1+maxa*d
        for i=1:an
            if x_0'*AM(:,i)==b(i)
                I(i)=i;
            else
                I(i)=0;
            end
        end
        I1=find(I)
        n=size(I1,2);
        A=[];
        for i=1:n
            A=[A AM(:,I1(i))];
        end
        A
    end
    
    x_1=x_0
    x_0=newpoint(I1,A,b,f,x1,x2)
    k=k+1;
end %End of while
%%
% The Result
disp 'The Best Result:'
xbest
disp 'Correlative Assistant Parameter: '
u=u'

