import numpy as np
from sympy import *


# Helper functions
def setdiff(set1, set2):
    if isinstance(set1, list):
        output = list()
        for item in set1:
            if item not in set2:
                output.append(item)
    elif isinstance(set1, set):
        output = set()
        for item in set1:
            if item not in set2:
                output.add(item)
    elif isinstance(set1, np.ndarray):
        output = np.array([])
        set1 = np.array(set1.flatten())
        set2 = np.array(set2.flatten())
        if isinstance(set1[0], np.ndarray):
            set1 = set1[0]
        if isinstance(set2[0], np.ndarray):
            set2 = set2[0]
        for item in set1:
            if item not in set2:
                output = np.append(output, item)
    else:
        raise TypeError("setdiff only supports list or set")
    return list(set(output))


def new_point(I1, A, b, f, x1, x2):
    num = len(I1)  # Number of columns in I1
    if num == 1:
        # A_t = A.transpose()  # Transpose because matlab is column based by numpy.matrix is row based
        rn, cn = np.where(A > 0)
        m = list(zip(rn, cn))  # A list of indices where A > 0
        ms = len(m)
        if ms == 1:
            x_0 = np.vstack((x1, x2))  # Vertically stack x1 and x2 together
            x_0[m[0]] = -b[I1]
            q = f.subs(x1, x_0[0]).subs(x1, x_0[1])
            xmin = solve(np.diff(q))
            if m == 1:
                x_0[1] = xmin
            else:
                x_0[0] = xmin
        else:
            x_0 = np.vstack((x1, x2))
            x_1[0] = (b[I1] - A[1] * x_0[1]) / A[0]
            p = simplify(f.subs(x1, x_0[0]).subs(x_1, x_0[1]))
            x2 = solve(np.diff(p))
            x1 = (b[I1] - A[1] * x2) / A[0]
            x_0 = np.vstack((x1, x2))
    else:
        bb = b[list(map(int, I1))]
        x_0 = np.linalg.solve(A, bb.transpose())
        x_0 = np.matrix(x_0).transpose()
    return x_0


# objective function
# #ex8
# f=0.5*x1^2+0.5*x2^2+x2+2*x1;
# example 5
x1 = Symbol('x1')
x2 = Symbol('x2')

f = Matrix([x1 ** 2 + x2 ** 2 - x1 * x2 - 3 * x1])

g = f.jacobian(np.vstack((x1, x2)))
G = Matrix(np.vstack((g[0], g[1]))).jacobian([x1, x2])

# Initial Value x_0
# #ex8
# x_0=[5;0];
# example 5
x_0 = np.matrix([[0], [0]], dtype=float)

# #ex8
# AM=[1,0,-1,1,-1,0;1,-1,-1,-1,0,1] %constraint Coefficient Matrix
# b=[0 -2 -5 -2 -5 -1]; %Constant right hand value
# I=[0 0 3 0 5 0]; % initial active set
# example 5
AM = np.matrix([[1, -1, 0, 1], [1, 0, -1, 0]], dtype=float)
b = np.array([2, 0, 0, 3/2])
I = np.array([0, 2, 3, 0])

# # of constraints
an = np.size(AM, 1)

# Find the active set in I. Note that I is an array. I1 will always be a list
I1 = np.where(I > 0)[0]

# Define Matrix A
# The number of available aggregate
n = len(I1)  # Original matlab code: n = size(I1, 2) which would always result in 1. Think it's a typo

# I1(i): the active set
# output A according to I1(i) AM(:, I1(i))

A = []
for i in range(0, n):
    if not len(A):
        A = AM[:, I1[i]]
    else:
        A = np.hstack((A, AM[:, I1[i]]))


# Main loop
# Initialize
k = 0
N = 10  # number of iteration
x_1 = x_0
while k < N:
    print('Current iteration: ' + str(k))
    X = x_0
    c = 0
    for i in range(0, an):
        if X.transpose().dot(AM[:, i]) < b[i]:
            c += 1
    print("c=" + str(c))

    # If it is in the feasible region
    if not c:
        for i in range(0, an):
            if X.transpose().dot(AM[:, i]) == b[i]:
                I[i] = i
            else:
                I[i] = 0
        I1 = np.where(I > 0)[0]

        g = f.jacobian(np.vstack((x1, x2)))
        g = g.subs(x1, X[0]).subs(x2, X[1])
        # Transpose g
        g = g.transpose()
        G = G.subs(x1, X[0]).subs(x2, X[1])

        O = np.matrix(np.zeros((n, n)), dtype=float)
        Ap = np.vstack((np.hstack((G, -A)), np.hstack((A.transpose(), O))))
        # bo denotes zero vector in s.t.
        bgn = np.size(Ap, 0) - 2
        bgo = np.matrix(np.zeros((bgn, 1)), dtype=float)
        bg = np.vstack((-g, bgo))
        # Trying to fix some typing issue
        Ap = Ap.astype(np.float64, copy=False)
        bg = bg.astype(np.float64, copy=False)

        # Solving equations
        r = np.linalg.solve(Ap, bg)  # This requires Ap to be square. if this has problem change to linalg.lstsq
        rn = np.size(r, 0)
        # B is u in example
        B = r[2:rn]
        u = np.zeros(an)
        lt = rn - 2
        for i in range(0, lt):
            u[i] = B[i, 0]
        min_u = min(u)
        if min_u >= 0:
            xbest = x_0
            break
        else:
            s = -1  # Initialize s value
            for i in range(0, an):
                if u[0, i] == min_u:
                    s = i
                    break
            if s != -1:
                I[s] = 0

            I1 = setdiff(I1, np.array([s]))
            n = len(I1)
            A = []
            for i in range(0, n):
                if not len(A):
                    A = AM[:, int(I1[i])]
                else:
                    A = np.hstack((A, AM[:, int(I1[i])]))

    # Not in feasible region
    if c != 0:
        d = x_0 - x_1
        mu = np.zeros(an)
        alpha = np.zeros(an)
        for i in range(0, an):
            if AM[:, i].transpose() * d < 0:
                mu[i] = i
            if mu[i] > 0:
                alpha[i] = (b[mu[i]] - AM[:, mu[i]].transpose * x_1) / (AM[:, mu[i]].transpose() * d)
            else:
                alpha[i] = 100
        maxa = min(alpha)
        x_0 = x_1 + maxa * d
        for i in range(0, an):
            if x_0.transpose() * AM[:, i] == b[i]:
                I[i] = i
            else:
                I[i] = 0
        I1 = np.where(I > 0)[0]
        n = len(I1)
        A = []
        for i in range(0, n):
            if not len(A):
                A = AM[:, I1[i]]
            else:
                A = np.hstack((A, AM[:, I1[i]]))

    x_1 = x_0
    x_0 = new_point(I1, A, b, f, x1, x2)
    k += 1


print("The best result:" + str(xbest))
print("Correlative Assitant Parameters: ")
print(u)
