function x_0=newpoint(I1,A,b,f,x1,x2)

num=size(I1,2);
if num==1
    m=find(A);
    ms=size(m,1);
    if ms==1
        x_0=[x1;x2];
        x_0(m)=-b(I1);
        q=subs(f,{x1,x2},{x_0(1),x_0(2)});
        xmin=solve(diff(q));
        if m==1
            x_0(2)=xmin;
            return
        else
            x_0(1)=xmin;
            return
        end
    else
        x_0=[x1;x2];
        x_0(1)=(b(I1)-A(2)*x_0(2))/A(1);
        p=simplify(subs(f,{x1,x2},{x_0(1),x_0(2)}));
        x2=solve(diff(p));
        x1=(b(I1)-A(2)*x2)/A(1);
        x_0=[x1;x2];
    end
else
    bb=b(I1);
    x_0=A\bb';
end
end